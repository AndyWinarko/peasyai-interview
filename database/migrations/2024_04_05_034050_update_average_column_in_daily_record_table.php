<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('daily_record', function (Blueprint $table) {
            $table->integer('male_avg_age')->nullable()->change();
            $table->integer('female_avg_age')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('daily_record', function (Blueprint $table) {
            //
            $table->integer('male_avg_age')->nullable(false)->change();
            $table->integer('female_avg_age')->nullable(false)->change();
        });
    }
};
