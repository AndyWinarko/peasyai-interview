<?php

namespace Domain\Users\Actions;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class UpsertUserRecordAction
{
    public function execute(array $userData)
    {
        $hourlyRecord = [
            'maleCount' => 0,
            'femaleCount' => 0,
        ];

        foreach ($userData as $item) {
            $user = User::query()->updateOrCreate(
                ['id' => $item['login']['uuid']],
                [
                    'gender' => $item['gender'],
                    'location' => json_encode($item['location']),
                    'age' => $item['dob']['age'],
                    'name' => json_encode($item['name']),
                    'email' => $item['email'],
                    'password' => $item['login']['password'],
                ]
            );

            if ($user->wasRecentlyCreated) {
                $user->gender == 'male'
                    ? $hourlyRecord['maleCount']++
                    : $hourlyRecord['femaleCount']++;

                Log::info('User created: '.$user->id);
            }
        }

        $carbon = Carbon::now();
        $key = 'hourlyRecord:'.$carbon->format('d:H');

        Redis::set($key, serialize($hourlyRecord));
    }
}
