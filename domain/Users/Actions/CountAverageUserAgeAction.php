<?php

namespace Domain\Users\Actions;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class CountAverageUserAgeAction
{

    public function execute(): array
    {
        return User::query()
            ->select('gender', DB::raw('ROUND(AVG(age)) AS average_age'))
            ->groupBy('gender')
            ->pluck('average_age', 'gender')
            ->toArray();
    }
}
