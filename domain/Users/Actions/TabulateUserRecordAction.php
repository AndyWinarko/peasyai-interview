<?php

namespace Domain\Users\Actions;

use App\Models\DailyRecord;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;

class TabulateUserRecordAction
{
    public function execute()
    {
        $userRecords = $this->getUserRecordFromRedis();

        [$maleCount, $femaleCount] = $this->countUserPerDay(collect($userRecords));

        DailyRecord::query()->create([
            'male_count' => $maleCount,
            'female_count' => $femaleCount,
        ]);
    }

    private function countUserPerDay(Collection $userRecords): array
    {
        $male = $female = 0;

        $userRecords = $userRecords->filter(function ($record) {
            return $record !== null;
        });

        foreach ($userRecords as $record) {
            $record = unserialize($record);

            $male += $record['maleCount'];
            $female += $record['femaleCount'];
        }

        return [$male, $female];
    }

    private function getUserRecordFromRedis(): array
    {
        $keys = $this->generateRedisKeyToday();
        $values = Redis::mget($keys);

        return array_combine($keys, $values);
    }

    private function generateRedisKeyToday(): array
    {
        $day = Carbon::today()->format('d');

        for ($i = 0; $i < 24; $i++) {
            $redisKey[] = "hourlyRecord:$day:".str_pad($i, 2, '0', STR_PAD_LEFT);
        }

        return $redisKey;
    }
}
