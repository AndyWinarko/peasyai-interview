<?php

namespace Domain\Users\Services;

use Domain\Users\Concerns\BuildBaseRequest;

class RandomUserService
{
    use BuildBaseRequest;

    public function __construct(
        private readonly string $baseUrl,
    ) {
    }

    public function getUser(int $limit = 20): array
    {
        $response = $this->buildBaseUrl()
            ->timeout(seconds: 15)
            ->get(url: "?results={$limit}");

        return json_decode($response, true);
    }
}
