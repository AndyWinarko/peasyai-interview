FROM php:8.3-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apt-get update \
  && apt-get install -y build-essential zlib1g-dev default-mysql-client curl gnupg procps vim git unzip libzip-dev libpq-dev supervisor \
  && docker-php-ext-install zip pdo_mysql pdo_pgsql pgsql

# redis
RUN pecl install redis && docker-php-ext-enable redis

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Set working directory
WORKDIR /var/www

# Copy Supervisor configuration file
COPY ./docker-compose/supervisor/laravel-worker.conf /etc/supervisor/conf.d/

USER $user

# Start Supervisor and PHP-FPM
# CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
CMD ["supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf", "-e", "debug"]
