<?php

namespace App\Http\Controllers;

use Domain\Users\Actions\TabulateUserRecordAction;
use Domain\Users\Actions\UpsertUserRecordAction;
use Domain\Users\Services\RandomUserService;
use Illuminate\Http\Request;
use App\Models\User;

class RandomUserController extends Controller
{

    public function index()
    {
        $users = User::query()->paginate(10);
        return view(
            'user',
            compact('users')
        );

    }

    public function remove(Request $request)
    {
        $request->input('id');

        User::query()->where('id', $request->input('id'))->delete();

        return response()->json([
            'message' => 'User removed successfully',
        ]);
    }

    /**
     * simulate job process save to db.
     */
    public function insert(UpsertUserRecordAction $action, RandomUserService $api)
    {
        $users = $api->getUser(limit: 20);

        $action->execute($users['results']);

        return response()->json([
            'message' => 'ProcessSaveUserRecordHourly job dispatched',
        ]);
    }

    public function tabulateAction(TabulateUserRecordAction $action)
    {
        $result = $action->execute();

        return response()->json($result);
    }
}
