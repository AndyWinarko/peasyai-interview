<?php

namespace App\Http\Controllers;

use App\Models\DailyRecord;
use App\Models\User;

class HelperController extends Controller
{
    public function show()
    {
        return User::count();
    }

    public function delete()
    {
        User::query()->delete();
    }

    public function showDailyRecord()
    {
        return DailyRecord::all();
    }

}
