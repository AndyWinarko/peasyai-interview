<?php

namespace App\Models;

use App\Events\CalculateAverageAge;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyRecord extends Model
{
    use HasFactory;

    protected $table = 'daily_record';

    protected $fillable = [
        'male_count',
        'female_count',
        'male_avg_age',
        'female_avg_age',
    ];

    protected $dispatchesEvents = [
        'updating' => CalculateAverageAge::class,
    ];
}
