<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Domain\Users\Actions\TabulateUserRecordAction;
use Illuminate\Support\Facades\Log;

class ProceedTabulateDataDaily implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(TabulateUserRecordAction $tabulateUserRecordAction): void
    {
        Log::info('ProceedTabulateDataDaily job started');
        $tabulateUserRecordAction->execute();
        Log::info('ProceedTabulateDataDaily job finished');
    }
}
