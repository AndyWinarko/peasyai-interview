<?php

namespace App\Jobs;

use Domain\Users\Actions\UpsertUserRecordAction;
use Domain\Users\Services\RandomUserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessSaveUserRecordHourly implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        Log::info('ProcessSaveUserRecordHourly job initialized');
    }

    /**
     * Execute the job.
     */
    public function handle(
        RandomUserService $api,
        UpsertUserRecordAction $upsertUserRecordAction
    ): void {
        Log::info('ProcessSaveUserRecordHourly job started');

        $users = $api->getUser(limit: 20);

        $upsertUserRecordAction->execute($users['results']);

        Log::info('ProcessSaveUserRecordHourly job finished');
    }
}
