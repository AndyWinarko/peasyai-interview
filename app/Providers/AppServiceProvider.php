<?php

namespace App\Providers;

use Domain\Users\Services\RandomUserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->singleton(
            abstract: RandomUserService::class,
            concrete: fn () => new RandomUserService(
                baseUrl: strval(config('services.randomuserapi.url'))
            )
        );
    }
}
