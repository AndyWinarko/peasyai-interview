<?php

namespace App\Events;

use Domain\Users\Actions\CountAverageUserAgeAction;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CalculateAverageAge
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(Model $model)
    {
        Log::info('CalculateAverageAge event fired from '.$model->getTable().' model');
        $average = (new CountAverageUserAgeAction())->execute();

        $model->male_avg_age = $average['male'];
        $model->female_avg_age = $average['female'];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('channel-name'),
        ];
    }
}
