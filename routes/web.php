<?php

use App\Http\Controllers\RandomUserController;
use Illuminate\Support\Facades\Route;
use Spatie\Health\Http\Controllers\HealthCheckResultsController;
use App\Jobs\ProcessSaveUserRecordHourly;
use App\Models\User;

Route::get('/', [RandomUserController::class, 'index']);
Route::DELETE('remove', [RandomUserController::class, 'remove']);

Route::get('health', HealthCheckResultsController::class);

Route::get('test', function () {
    ProcessSaveUserRecordHourly::dispatch();
    return 'Dispatched';
    // $name = [
    //     'surname' => 'Test',
    //     'name' => 'Test',
    //     'username' => 'Test',
    // ];

    // User::create([
    //     'id' => uuid_create(),
    //     'name' => json_encode($name),
    //     'email' => 'testing@gmail.com',
    //     'password' => 'password',
    //     'gender' => 'male',
    //     'location' => json_encode(''),
    //     'age' => 20
    // ]);

    // User::query()->delete();
    // return User::all();
});
