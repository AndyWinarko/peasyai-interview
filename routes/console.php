<?php

use App\Jobs\ProcessSaveUserRecordHourly;
use App\Jobs\ProceedTabulateDataDaily;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schedule;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();

Schedule::job(new ProcessSaveUserRecordHourly)->hourly();
Schedule::job(new ProceedTabulateDataDaily)->dailyAt('11:05');
